import React, { Component } from 'react';
import './App.css';
import { CardList } from './components/card-list/card-list.component'
import { SearchBox } from './components/search-box/search-box.component'
import { Loading } from './components/loading/loading.component'

class App extends Component {

  constructor() {
    super();

    this.state = {
      monsters: [],
      searchField: '',
      loading: true
    }
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then((users) => {
        setTimeout(() => {
          this.setState({ monsters: users, loading: false })
        }, 2500)
      })
  }

  handleChange = (event) => {
    this.setState({ searchField: event.target.value })
  }

  renderContainer(show, filteredMonster) {
    if (show) {
      return (
        <div>
          <Loading />
        </div>
      )
    }
    else {
      return (
        <div className="App">
          <h1 className="monsters-rolodex">Monsters Rolodex</h1>

          <SearchBox
            placeholder="Search Monsters"
            handleChange={e => this.handleChange(e)}
          />

          <CardList monsters={filteredMonster} />
        </div>
      )
    }
  }


  render() {

    const { monsters, searchField } = this.state;

    const filteredMonsters = monsters.filter(monster =>
      monster.name.toLowerCase().includes(searchField.toLowerCase())
    )


    return (
      <div className="App">
        {this.renderContainer(this.state.loading, filteredMonsters)}
      </div>
    )
  }
}

export default App;
