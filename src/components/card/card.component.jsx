import React from 'react'
import './card.styles.css'

export const Card = (props) => {
    return (
        <div className="card-container">
            <img src={`https://robohash.org/${props.img}?set=set2&size=180x180`} alt="monster" />
            <h1>{props.monster}</h1>
            <p>{props.email}</p>
        </div>
    )
}